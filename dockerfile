# Utilisation de l'image Apache HTTPD officielle comme base
FROM httpd:latest

# Téléchargez le contenu du site web statique depuis GitLab
RUN apt-get update && apt-get install -y git
RUN git clone https://gitlab.com/mog4dor/ElectroShop.git /usr/local/src

# Exposez le port 80 pour le serveur HTTPD
EXPOSE 80

# Commande de démarrage du serveur HTTPD
CMD ["httpd", "-D", "FOREGROUND"]
 